package fr.ign.addressdb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button bForm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Récupération des composants graphiques en variables Java
        bForm = findViewById(R.id.bForm);
        Button bMaps = findViewById(R.id.bMaps);
        // Ajout des écouteurs d'événement
        bForm.setOnClickListener(view -> {
            Intent intent = new Intent(this, FormActivity.class);
            startActivity(intent);
        });
        bMaps.setOnClickListener(view -> {
            Intent intent = new Intent(this, MapsActivity.class);
            startActivity(intent);
        });

        // Récupération db
        AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "database-name")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
        // Récupération DAO (à mettre en attribut de classe si utilisé ailleurs...)
        AddressDao addressDao = db.addressDao();


        // Ajout d'une entité
        // Address address = new Address("adresse", 42.0f, 3.0f);
        // addressDao.insert(address);
        // Listing des entités
        List<Address> addresses = addressDao.getAll();
        // Log (implémentez la fonction toString !!!)
        Log.i("ENSG", addresses.toString());
    }
}