package fr.ign.addressdb;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.android.gms.maps.model.LatLng;

@Entity(tableName="address")
public class Address {
    @PrimaryKey(autoGenerate = true)
    public int uid;
    @ColumnInfo
    public String address;
    @ColumnInfo
    public Float lat;
    @ColumnInfo
    public Float lng;

    // Constructeur utilisé au moment de l'insertion.
    // On doit renseigner les données métier.
    public Address(String address, Float lat, Float lng) {
        this.address = address;
        this.lat = lat;
        this.lng = lng;
    }

    public LatLng getLatLng() {
        return new LatLng(lat, lng);
    }

    @Override
    public String toString() { // __str__()
        return "Address{" +
                "uid=" + uid +
                ", address='" + address + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                '}';
    }
}
