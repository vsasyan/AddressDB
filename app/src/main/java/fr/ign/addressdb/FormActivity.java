package fr.ign.addressdb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.location.Geocoder;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

public class FormActivity extends AppCompatActivity {
    EditText etAddress;
    TextView tvLatLng;
    AddressDao addressDao;
    // Données à insérer
    double lat;
    double lng;
    String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        // Récupération des composants
        etAddress = findViewById(R.id.etAddress);
        tvLatLng = findViewById(R.id.tvLatLng);
        Button bGeocode = findViewById(R.id.bGeocoder);
        Button bSave = findViewById(R.id.bSave);
        // Ajout des écouteurs
        bGeocode.setOnClickListener(view -> {
            geocode();
        });
        bSave.setOnClickListener(view -> {
            save();
        });


        // Récupération db
        AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "database-name")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
        // Récupération DAO (à mettre en attribut de classe si utilisé ailleurs...)
        addressDao = db.addressDao();
    }

    private void geocode() {
        // On récupère l'adresse de l'utilsiateur
        String userAddress = etAddress.getText().toString();
        // Instanciation du geocoder
        Geocoder geocoder = new Geocoder(this);
        // Utilisation de Geocode pour compléter l'adresse
        try {
            // Récupération de la liste d'adresse GOOGLE complétée
            List<android.location.Address> geoAddresses = geocoder.getFromLocationName(userAddress, 1);
            // Récupération lat/lng
            if (!geoAddresses.isEmpty()) {
                // Récupération Lat/Lng
                lat = geoAddresses.get(0).getLatitude(); // A stocker
                lng = geoAddresses.get(0).getLongitude(); // A stocker
                address = geoAddresses.get(0).getAddressLine(0); // TODO : boucler sur les lignes en utilisant .getMaxAddressLineIndex() cf. TP Carto
                // Maj adresse
                etAddress.setText(address);
                tvLatLng.setText(String.format("address = %s, lat = %f, lng = %f", this.address, lat, lng));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void save() {
        // Instancier la BDD (dans le onCreate)
        // Récupérer le DAO (aussi dans le onCreate, en attribut)
        // Instancier une adresse à partir des info à sauver
        Address newAddress = new Address(this.address, (float)this.lat, (float)this.lng);
        // Insérer l'adresse
        addressDao.insert(newAddress);
        // Affichage Toast
        Toast.makeText(this, "Enregistrement OK.", Toast.LENGTH_SHORT).show();
        // reset le formulaire
        etAddress.setText("");
        tvLatLng.setText("");
    }
}