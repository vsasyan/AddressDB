package fr.ign.addressdb;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Address.class}, version = 3)
public abstract class AppDatabase extends RoomDatabase {
    public abstract AddressDao addressDao();
}

