package fr.ign.addressdb;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface AddressDao {
    @Query("SELECT * FROM address")
    List<Address> getAll();
    @Insert
    void insert(Address address);
}
